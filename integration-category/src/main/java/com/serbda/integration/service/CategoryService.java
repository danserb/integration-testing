package com.serbda.integration.service;

import com.serbda.integration.model.Category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CategoryService {

    List<Category> findAll();

    Optional<Category> findById(UUID id);

}
