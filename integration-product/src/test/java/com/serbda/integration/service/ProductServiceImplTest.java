package com.serbda.integration.service;

import com.serbda.integration.client.CategoryClient;
import com.serbda.integration.exception.ProductNotFoundException;
import com.serbda.integration.model.CategoryEntity;
import com.serbda.integration.model.ProductEntity;
import com.serbda.integration.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

    @Mock
    private ProductRepository repository;

    @Mock
    private CategoryClient categoryClient;
    @InjectMocks
    private ProductServiceImpl productService;

    private ProductEntity productOne;
    private ProductEntity productTwo;

    @BeforeEach
    void setUp() {
        productOne = ProductEntity.builder()
                .id(UUID.randomUUID())
                .name("name1")
                .description("description1")
                .price(BigDecimal.TEN)
                .categoryId(UUID.randomUUID())
                .build();

        productTwo = ProductEntity.builder()
                .id(UUID.randomUUID())
                .name("name2")
                .description("description2")
                .price(BigDecimal.TEN)
                .categoryId(UUID.randomUUID())
                .build();
    }

    @Test
    public void addProductTest_successful() {
        when(repository.save(productOne)).thenReturn(productOne);

        ProductEntity savedProduct = productService.add(productOne);

        assertEquals(productOne, savedProduct);
        verify(repository, Mockito.times(1)).save(productOne);
    }

    @Test
    public void deleteByIdTest_successful() {
        UUID id = UUID.randomUUID();
        when(repository.findById(id)).thenReturn(Optional.of(productOne));

        productService.deleteById(id);

        verify(repository, Mockito.times(1)).findById(id);
        verify(repository, Mockito.times(1)).deleteById(id);
    }

    @Test
    public void deleteByIdTest_failure() {
        UUID id = UUID.randomUUID();

        assertThrows(ProductNotFoundException.class, () -> productService.deleteById(id));
        verify(repository, times(0)).deleteById(id);
    }

    @Test
    public void getAllProductsTest() {
        CategoryEntity dummyCategory = CategoryEntity.builder()
                .id(productOne.getCategoryId())
                .name("name")
                .description("description")
                .build();

        List<ProductEntity> expectedProducts = List.of(productOne, productTwo);
        when(repository.findAll()).thenReturn(expectedProducts);
        when(categoryClient.getCategoryById(anyString())).thenReturn(dummyCategory);

        List<ProductEntity> allProducts = productService.getAll();

        verify(repository, times(1)).findAll();
        assertEquals(expectedProducts, allProducts);
        allProducts.forEach(productEntity -> assertEquals(dummyCategory, productEntity.getCategory()));

    }

}
