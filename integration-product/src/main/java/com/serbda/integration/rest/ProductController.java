package com.serbda.integration.rest;

import com.serbda.integration.dto.Product;
import com.serbda.integration.dto.mapper.ProductMapper;
import com.serbda.integration.model.ProductEntity;
import com.serbda.integration.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/v1/products")
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    public ProductController(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @Valid
    @PostMapping
    public ResponseEntity<Product.ResponseBody.Created> addProduct(@Valid @RequestBody Product.RequestBody.Create product) {
        ProductEntity freshProduct = productMapper.mapCreateProductRequestToProduct(product);
        ProductEntity savedProduct = productService.add(freshProduct);
        return ResponseEntity.ok(productMapper.mapProductToCreate(savedProduct));
    }

    @Valid
    @PutMapping(value = "/{id}")
    public ResponseEntity<Product.ResponseBody.Update> updateProduct(@Valid @RequestBody Product.RequestBody.Update product, @Valid @PathVariable String id) {
        UUID productToUpdateId = UUID.fromString(id);
        ProductEntity productToUpdate = productMapper.mapUpdateProductRequestToProduct(product, productToUpdateId);
        ProductEntity updatedProduct = productService.update(productToUpdate, productToUpdateId);
        return ResponseEntity.ok(productMapper.mapProductToUpdate(updatedProduct));
    }

    @Valid
    @GetMapping
    public ResponseEntity<List<Product.ResponseBody.FindAll>> getProducts() {
        List<ProductEntity> products = productService.getAll();
        List<Product.ResponseBody.FindAll> responseProducts = products.stream()
                .map(productMapper::mapProductToFindAll)
                .collect(Collectors.toList());
        return ResponseEntity.ok(responseProducts);
    }

    @GetMapping(value = "/stress")
    public ResponseEntity<List<Product.ResponseBody.FindAll>> get5120Products() {
        List<ProductEntity> products = productService.getALotOfProducts();
        List<Product.ResponseBody.FindAll> responseProducts = products.stream()
                .map(productMapper::mapProductToFindAll)
                .collect(Collectors.toList());
        return ResponseEntity.ok(responseProducts);
    }

    @Valid
    @GetMapping(value = "/{id}")
    public ResponseEntity<Product.ResponseBody.GetById> getProductById(@Valid @PathVariable String id) {
        UUID productId = UUID.fromString(id);
        ProductEntity productEntity = productService.getById(productId);
        return ResponseEntity.ok(productMapper.mapProductToGetById(productEntity));
    }

    @Valid
    @DeleteMapping(value = "/{id}")
    public void deleteProductById(@Valid @PathVariable String id) {
        UUID productId = UUID.fromString(id);
        productService.deleteById(productId);
    }
}
