package com.serbda.integration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "products")
public class ProductEntity {

    @Id
    @GeneratedValue()
    private UUID id;
    private String name;
    private String description;
    private BigDecimal price;
    @Column(name = "category_id")
    private UUID categoryId;
    @Transient
    private CategoryEntity category;

}
