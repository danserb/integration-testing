package com.serbda.integration.service;

import com.serbda.integration.model.ProductEntity;

import java.util.List;
import java.util.UUID;

public interface ProductService extends CrudService<ProductEntity> {

    ProductEntity update(ProductEntity product, UUID id);

    List<ProductEntity> getALotOfProducts();
}
