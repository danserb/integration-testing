package com.serbda.integration.service;

import com.serbda.integration.client.CategoryClient;
import com.serbda.integration.exception.ProductNotFoundException;
import com.serbda.integration.model.CategoryEntity;
import com.serbda.integration.model.ProductEntity;
import com.serbda.integration.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryClient categoryClient;

    public ProductServiceImpl(ProductRepository productRepository, CategoryClient categoryClient) {
        this.productRepository = productRepository;
        this.categoryClient = categoryClient;
    }

    @Override
    public ProductEntity add(ProductEntity product) {
        return productRepository.save(product);
    }

    @Override
    public ProductEntity update(ProductEntity product, UUID id) {
        product.setId(id);
        Optional<ProductEntity> maybeProduct = productRepository.findById(id);
        return maybeProduct.map(productEntity -> {
            productEntity.setName(product.getName());
            productEntity.setDescription(product.getDescription());
            productEntity.setCategoryId(product.getCategoryId());
            productEntity.setPrice(product.getPrice());
            return productRepository.save(productEntity);
        }).orElseThrow(() -> new ProductNotFoundException("Error! Product not found!"));

    }

    @Override
    public List<ProductEntity> getALotOfProducts() {
        List<ProductEntity> aLotOfProducts = productRepository.findAll();

        for (int i=0; i<10; i++) {
            aLotOfProducts.addAll(aLotOfProducts);
        }

        aLotOfProducts.forEach(product -> {
            CategoryEntity category = categoryClient.getCategoryById(product.getCategoryId().toString());
            product.setCategory(category);
        });

        return aLotOfProducts;
    }

    @Override
    public ProductEntity getById(UUID id) {
        Optional<ProductEntity> maybeProduct = productRepository.findById(id);
        maybeProduct.ifPresent(productEntity -> {
            CategoryEntity categorySummary = categoryClient.getCategoryById(productEntity.getCategoryId().toString());
            productEntity.setCategory(categorySummary);
        });
        return maybeProduct.orElseThrow(() -> new ProductNotFoundException("Error! Product not found!"));
    }

    @Override
    public void deleteById(UUID id) {
        Optional<ProductEntity> maybeProduct = productRepository.findById(id);
        maybeProduct.ifPresentOrElse(productEntity -> productRepository.deleteById(productEntity.getId()),
                () -> {
                    throw new ProductNotFoundException("Error! Product not found!");
                });
        productRepository.deleteById(id);
    }

    @Override
    public List<ProductEntity> getAll() {
        List<ProductEntity> products = productRepository.findAll();
        products.forEach(product -> {
            CategoryEntity category = categoryClient.getCategoryById(product.getCategoryId().toString());
            product.setCategory(category);
        });
        return products;
    }

}
