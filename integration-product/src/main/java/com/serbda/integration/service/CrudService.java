package com.serbda.integration.service;

import java.util.List;
import java.util.UUID;

public interface CrudService<T> {

    List<T> getAll();

    T add(T t);

    T getById(UUID id);

    void deleteById(UUID id);
}
