package com.serbda.integration.dto.mapper;

import com.serbda.integration.dto.Product;
import com.serbda.integration.model.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ProductMapper {

    public Product.ResponseBody.FindAll mapProductToFindAll(ProductEntity product) {
        return new Product.ResponseBody.FindAll(product.getId(), product.getName(), product.getPrice(), product.getCategory());
    }

    public Product.ResponseBody.Created mapProductToCreate(ProductEntity product) {
        return new Product.ResponseBody.Created(product.getId(), product.getName(), product.getDescription(), product.getPrice(), product.getCategoryId());
    }

    public Product.ResponseBody.Update mapProductToUpdate(ProductEntity product) {
        return new Product.ResponseBody.Update(product.getId(), product.getName(), product.getDescription(), product.getPrice(), product.getCategoryId());
    }

    public Product.ResponseBody.GetById mapProductToGetById(ProductEntity product) {
        return new Product.ResponseBody.GetById(product.getId(), product.getName(), product.getDescription(), product.getPrice(), product.getCategory());
    }

    public ProductEntity mapCreateProductRequestToProduct(Product.RequestBody.Create productRequest) {
        return ProductEntity.builder()
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .categoryId(productRequest.getCategoryId())
                .build();
    }

    public ProductEntity mapUpdateProductRequestToProduct(Product.RequestBody.Update productRequest, UUID id) {
        return ProductEntity.builder()
                .id(id)
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .categoryId(productRequest.getCategoryId())
                .build();
    }

}
