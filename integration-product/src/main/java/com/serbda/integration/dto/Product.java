package com.serbda.integration.dto;

import com.serbda.integration.model.CategoryEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.beans.ConstructorProperties;
import java.math.BigDecimal;
import java.util.UUID;

public enum Product {
    ;

    @Getter
    public static class Base implements Name, Description {

        String name;
        String description;

        public Base(String name, String description) {
            this.name = name;
            this.description = description;
        }
    }

    private interface Id {
        @NotNull
        UUID getId();
    }

    private interface Name {
        @NotNull
        @Size(min = 3, max = 35, message = "Please provide a name with a minimum of 3 and a maximum of 35 characters!")
        String getName();
    }

    private interface Description {
        @NotNull
        @Size(min = 3, max = 100, message = "Please provide a description with a minimum of 3 and a maximum of 100 characters!")
        String getDescription();
    }

    private interface Price {
        @Positive
        BigDecimal getPrice();
    }

    private interface CategoryId {
        @NotNull
        UUID getCategoryId();
    }

    private interface CategorySummary {
        @NotNull
        CategoryEntity getCategorySummary();

    }

    public enum RequestBody {
        ;

        @Value
        @EqualsAndHashCode(callSuper = true)
        public static class Create extends Base implements Price, CategoryId {

            BigDecimal price;
            UUID categoryId;

            @ConstructorProperties(value = {"name", "description", "price", "categoryId"})
            public Create(String name, String description, BigDecimal price, UUID categoryId) {
                super(name, description);
                this.price = price;
                this.categoryId = categoryId;
            }

        }

        @Value
        @EqualsAndHashCode(callSuper = true)
        public static class Update extends Base implements Price, CategoryId {

            BigDecimal price;
            UUID categoryId;

            @ConstructorProperties(value = {"name", "description", "price", "categoryId"})
            public Update(String name, String description, BigDecimal price, UUID categoryId) {
                super(name, description);
                this.price = price;
                this.categoryId = categoryId;
            }

        }

    }

    public enum ResponseBody {
        ;

        @Value
        @EqualsAndHashCode(callSuper = true)
        public static class Created extends Base implements Id, Price, CategoryId {

            UUID id;
            BigDecimal price;
            UUID categoryId;

            @ConstructorProperties(value = {"id", "name", "description", "price", "categoryId"})
            public Created(UUID id, String name, String description, BigDecimal price, UUID categoryId) {
                super(name, description);
                this.id = id;
                this.price = price;
                this.categoryId = categoryId;
            }

        }

        @Value
        @EqualsAndHashCode(callSuper = true)
        public static class Update extends Base implements Id, Price, CategoryId {

            UUID id;
            BigDecimal price;
            UUID categoryId;

            @ConstructorProperties(value = {"id", "name", "description", "price", "categoryId"})
            public Update(UUID id, String name, String description, BigDecimal price, UUID categoryId) {
                super(name, description);
                this.id = id;
                this.price = price;
                this.categoryId = categoryId;
            }

        }


        @Value
        public static class FindAll implements Id, Name, Price, CategorySummary {

            UUID id;
            String name;
            BigDecimal price;
            CategoryEntity categorySummary;

            @ConstructorProperties(value = {"id", "name", "price"})
            public FindAll(UUID id, String name, BigDecimal price, CategoryEntity categorySummary) {
                this.id = id;
                this.name = name;
                this.price = price;
                this.categorySummary = categorySummary;
            }
        }

        @Value
        @EqualsAndHashCode(callSuper = true)
        public static class GetById extends Base implements Id, Price, CategorySummary {

            UUID id;
            BigDecimal price;
            CategoryEntity categorySummary;

            @ConstructorProperties(value = {"id", "name", "description", "price", "categorySummary"})
            public GetById(UUID id, String name, String description, BigDecimal price, CategoryEntity categorySummary) {
                super(name, description);
                this.id = id;
                this.price = price;
                this.categorySummary = categorySummary;
            }
        }
    }

}
