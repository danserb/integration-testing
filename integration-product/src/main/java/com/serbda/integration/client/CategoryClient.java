package com.serbda.integration.client;

import com.serbda.integration.model.CategoryEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CategoryClient {

    @Value("${category.client.host}")
    String host;

    @Value("${category.client.port}")
    String port;

    @Value("${category.client.categories}")
    String categoryPath;

    private final RestTemplate restTemplate;

    public CategoryClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CategoryEntity getCategoryById(String categoryId) {
        ResponseEntity<CategoryEntity> categoryResponse = restTemplate.getForEntity(getCategoryURL().concat(categoryId), CategoryEntity.class);
        return categoryResponse.getBody();
    }

    private String getCategoryURL() {
        return host.concat(":").concat(port).concat(categoryPath);
    }

}
