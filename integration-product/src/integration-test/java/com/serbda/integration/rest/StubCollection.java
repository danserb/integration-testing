package com.serbda.integration.rest;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class StubCollection {

    public static StubMapping getCategoryByIdMatchOne() {
        return stubFor(get(urlEqualTo("/api/v1/categories/a477e522-8e98-11ea-bc55-0242ac130003"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\n" +
                                "\"id\": \"a477e522-8e98-11ea-bc55-0242ac130003\",\n" +
                                "\"name\": \"hobbies\",\n" +
                                "\"description\": \"dummy test description\"\n" +
                                "}")));
    }

    public static StubMapping getCategoryByIdMatchTwo() {
        return stubFor(get(urlEqualTo("/api/v1/categories/a477e748-8e98-11ea-bc55-0242ac130003"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\n" +
                                "\"id\": \"a477e748-8e98-11ea-bc55-0242ac130003\",\n" +
                                "\"name\": \"kitchen\",\n" +
                                "\"description\": \"dummy description\"\n" +
                                "}")));
    }
}
