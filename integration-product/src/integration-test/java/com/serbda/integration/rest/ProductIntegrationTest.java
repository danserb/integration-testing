package com.serbda.integration.rest;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(value = "test")
public class ProductIntegrationTest {

    @LocalServerPort
    int serverPort;

    private static WireMockServer server;

    @BeforeAll
    public static void init() {
        server = new WireMockServer(WireMockConfiguration.options()
                .port(8082));
        server.start();
        WireMock.configureFor("localhost", server.port());
        server.addStubMapping(StubCollection.getCategoryByIdMatchOne());
        server.addStubMapping(StubCollection.getCategoryByIdMatchTwo());
    }

    @AfterAll
    public static void cleanUp() {
        server.stop();
    }

    @Test
    public void testGetProductById_found() {

        given()
                .port(serverPort)
                .get("/api/v1/products/83476d78-8e98-11ea-bc55-0242ac130003")
                .then()
                .statusCode(200)
                .log().body(true)
                .body("categorySummary.name", equalTo("hobbies"));
    }

    @Test
    public void testGetAllProducts_valid() {
        given()
                .port(serverPort)
                .get("/api/v1/products/")
                .then()
                .statusCode(200)
                .log().body(true);
    }

    @Test
    public void testGet10000Products_allClientCallsSucceeded() {
        given()
                .port(serverPort)
                .get("api/v1/products/stress")
                .then()
                .statusCode(200)
                .body("categorySummary", notNullValue());
    }

}
